# dwm

This is my build of [dwm](https://dwm.suckless.org).

## Patches

* [dwm-xresources](https://dwm.suckless.org/patches/xresources/dwm-xresources-6.2.diff)
* [dwm-actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/dwm-actualfullscreen-20211013-cb3f58a.diff)
* [dwm-tilegap](https://dwm.suckless.org/patches/tilegap/dwm-tilegap-6.2.diff)
* [dwm-cfacts](https://dwm.suckless.org/patches/cfacts/dwm-cfacts-20200913-61bb8b2.diff)
* [dwm-status2d](https://dwm.suckless.org/patches/status2d/dwm-status2d-20200508-60bb3df.diff)
* [dwm-pertag](https://dwm.suckless.org/patches/pertag/dwm-pertag-20200914-61bb8b2.diff)
